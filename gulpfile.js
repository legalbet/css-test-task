const { src, dest, watch, series, parallel, lastRun } = require("gulp");
const del = require("del");
const autoprefixer = require("autoprefixer");
const browserSync = require("browser-sync").create();
const isDev = "development";
const plumber = require("gulp-plumber");
const gulpIf = require("gulp-if");
const sourcemaps = require("gulp-sourcemaps");
const sass = require("gulp-sass");
const postcss = require("gulp-postcss");
const pug = require("gulp-pug");
const imagemin = require("gulp-imagemin");

function server() {
  browserSync.init({
    notify: false,
    port: 9000,
    server: {
      baseDir: ["dist", "src"],
    },
  });
  watch("src/**/*.pug", templates);
  watch(["src/styles/**/*.scss", "src/blocks/**/*.scss"], styles);
  watch("src/scripts/**/*.js", scripts);
  watch(["src/img/**/*", "src/fonts/**/*"]).on("change", browserSync.reload);
}

function templates() {
  return src("src/pages/**/*.pug")
    .pipe(plumber())
    .pipe(pug({ pretty: true }))
    .pipe(dest("dist/"))
    .pipe(browserSync.reload({ stream: true }));
}

function styles() {
  return src(["src/styles/main.scss"])
    .pipe(plumber())
    .pipe(gulpIf(isDev, sourcemaps.init()))
    .pipe(
      sass
        .sync({
          outputStyle: "expanded",
          precision: 10,
          includePaths: ["."],
        })
        .on("error", sass.logError)
    )
    .pipe(postcss([autoprefixer()]))
    .pipe(gulpIf(isDev, sourcemaps.write()))
    .pipe(dest("dist/styles"))
    .pipe(browserSync.reload({ stream: true }));
}

function scripts() {
  return src("src/scripts/**/*.js")
    .pipe(plumber())
    .pipe(dest("dist/scripts"))
    .pipe(browserSync.reload({ stream: true }));
}
function images() {
  return src("src/img/**/*", { since: lastRun(images) })
    .pipe(imagemin())
    .pipe(dest("dist/img"));
}

function fonts() {
  return src("src/fonts/**/*").pipe(dest("dist/fonts"));
}

function clean() {
  return del(["dist"]);
}

const build = series(
  clean,
  parallel(templates, styles, scripts, images, fonts)
);
const develop = series(build, server);

exports.default = develop;
exports.build = build;
exports.server = server;
exports.templates = templates;
exports.styles = styles;
exports.scripts = scripts;
exports.images = images;
exports.fonts = fonts;
exports.clean = clean;
